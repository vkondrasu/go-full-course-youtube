### source link 
https://www.youtube.com/watch?v=yyUHQIec83I

## Notes

#### Switch statemt
```
city := "Hyderabad"

switch city {
    case "London","Berlin":
    //some way of executing
    case "Hyderabad", "London":
    //Indian
    case "Sinagapore":
    //Singapore business
    default:
    //Default way of handling
}
```
